const one = 1;
const two = 2;
const three = 3;
const homeScore = document.getElementById('home-score');
const guestScore = document.getElementById('guest-score');

// Takes in 2 arguments. 
// First one, the target as either homeScore or guestScore to determine which target to increase.
// Second one is the amount to increase by.
function addPoints(scoreTarget, points) {
    let parsedTarget = Number(scoreTarget.textContent);
    parsedTarget += points;
    return scoreTarget.textContent = parsedTarget;
}